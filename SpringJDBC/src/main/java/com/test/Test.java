package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		CustDao cobj = ctx.getBean("cdao", CustDao.class);
		
		Customer cst = new Customer(103, "Kohli", "blr");
		
		//cobj.saveCustomer(cst);
		
		//cobj.updateCustomer(cst);
		
		cobj.deleteCustomer(cst);
		
		System.out.println("Done");
	}
}
