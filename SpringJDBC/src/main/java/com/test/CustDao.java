package com.test;

import org.springframework.jdbc.core.JdbcTemplate;

public class CustDao {
	
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int saveCustomer(Customer cst)
	{
		String sql = "insert into customer values("+cst.getId()+", '"+cst.getName()+"', '"+cst.getCity()+"')";
		
		return jdbcTemplate.update(sql);
	}
	
	public int updateCustomer(Customer cst) {
		String sql = "update customer set c_name ='"+cst.getName()+"', c_city = '"+cst.getCity()+"' where c_id ="+cst.getId()+"  ";
		
		return jdbcTemplate.update(sql);
	}
	
	public int deleteCustomer(Customer cst) {
		String sql = "delete from customer where c_id ="+cst.getId()+"  ";
		
		return jdbcTemplate.update(sql);
	}

}
