package com.emp;

import java.util.Scanner;

public class Test {
	
	public static void main(String[] args) //throws PsalException,NsalException
	{
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter salary");
		
		double sal = sc.nextDouble();
		
		Validator v = new Validator();
		try {
			v.checkSal(sal);
		}
		catch(PsalException pe)
		{
			System.out.println("valid");
		}
		catch (NsalException ne) {
			System.out.println("invalid");
		}
	}

}
