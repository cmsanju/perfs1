package com.ths;

public class Exp1 extends Thread
{
	public void run()
	{
		System.out.println("i am from run() : "+Thread.currentThread().getName());
	}
	
	
	public static void main(String[] args) {
		
		Exp1 t1 = new Exp1();
		Exp1 t2 = new Exp1();
		Exp1 t3 = new Exp1();
		
		System.out.println("Default thread priority : "+t1.getPriority());
		System.out.println("Default thread name  :"+t1.getName());
		System.out.println(t2.getName());
		System.out.println(t3.getName());
		
		t1.setName("tansfer");
		t2.setName("withdraw");
		t3.setName("credit");
		
		System.out.println(t1.getName());
		System.out.println(t2.getName());
		System.out.println(t3.getName());
		
		t1.setPriority(MAX_PRIORITY);
		
		System.out.println(t1.getPriority());
		
		System.out.println(MAX_PRIORITY);
		System.out.println(NORM_PRIORITY);
		System.out.println(MIN_PRIORITY);
		
		t1.start();
	}
}








