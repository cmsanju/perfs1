package com.ths;

public class Exp6 extends Thread
{
	public void run()
	{
		System.out.println("system shutdown started....");
	}
	
	public static void main(String[] args) throws Exception
	{
		/*
		Runtime rt = Runtime.getRuntime();
		
		rt.addShutdownHook(new Exp6());
		
		try
		{
			Thread.sleep(5000);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		*/
		
		Runtime runTime = Runtime.getRuntime();
		 
        // Take input from user in seconds
       int  seconds = 5;

        // Retrieve current Runtime Environment
        Process processing
            = runTime.exec("shutdown -h -t " + seconds);

        // Shutting Down the System
        System.exit(0);
	}
}
