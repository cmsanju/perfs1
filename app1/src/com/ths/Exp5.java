package com.ths;

class C
{
	public void details(String name, int id)
	{
		System.out.println("ID : "+id+" Name : "+name);
	}
}

class D
{
	public String stdDetails(int id, String name, String clg)
	{
		return " ID : "+id+" Name : "+name+" College : "+clg;
	}
}

public class Exp5 implements Runnable
{

	@Override
	public void run() {
		
		try
		{
			C c = new C();
			c.details("Hero", 111);
			
			Thread.sleep(3000);
			
			D d = new D();
			System.out.println(d.stdDetails(101, "King", "ABC"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		Exp5 t1 = new Exp5();
		
		Thread t2 = new Thread(t1);
		
		t2.start();
	}
	
}









