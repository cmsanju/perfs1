package com.ths;

class Add
{
	public void add()
	{
		System.out.println("Add : "+(20+30));
	}
}

class Sub
{
	public void sub()
	{
		System.out.println("Sub : "+(300-33));
	}
}

public class Exp2 extends Thread
{
	@Override
	public void run()
	{
		try
		{
			Add ad = new Add();
			ad.add();
			
			Thread.sleep(2000);
			
			Sub sb = new Sub();
			sb.sub();
					
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		Exp2 t1  = new Exp2();
		
		t1.start();
	}
}






