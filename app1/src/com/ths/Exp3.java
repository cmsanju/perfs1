package com.ths;

public class Exp3 extends Thread
{
	public void run()
	{
		try
		{
			for(int i = 1; i <= 10; i++)
			{
				System.out.println(i);
				if(i == 5) {
				Thread.sleep(2000);
				Thread.yield();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		Exp3 t1 = new Exp3();
		
		t1.start();
		
		Exp3 t2 = new Exp3();
		
		t2.start();
	}
}
