package com.ths;

public class Exp4 implements Runnable
{
	@Override
	public void run() {
		
		System.out.println("I am from runnable");
		
	}
	
	public static void main(String[] args) {
	
		Exp4 t1 = new Exp4();
		
		//t1.start();
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		Thread t2 = new Thread(tg1, t1,"Transfer");
		Thread t3 = new Thread(tg1, t1,"withdraw");
		Thread t4 = new Thread(tg1, t1,"credit");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
		
		Thread t5 = new Thread(tg2, t1,"Add");
		Thread t6 = new Thread(tg2, t1,"Sub");
		Thread t7 = new Thread(tg2, t1, "Mul");
		
		
		t3.start();
		t4.start();
		System.out.println(tg1.activeCount());
		t5.start();
		t7.start();
		System.out.println(tg2.activeCount());
		
	}
}





