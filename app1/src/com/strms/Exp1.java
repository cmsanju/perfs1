package com.strms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.cls.Employee;

public class Exp1 {
	
	public static void main(String[] args) {
		
		List<String> listNames = new ArrayList<>();
		
		listNames.add("devesh");
		listNames.add("umar");
		listNames.add("rohit");
		listNames.add("balaji");
		listNames.add("venkatesh");
		listNames.add("balaji");
		listNames.add("umar");
		listNames.add("adithya");
		listNames.add("devesh");
		listNames.add("balaji");
		
		Employee emp1 = new Employee(101, "Java", 22, "E Y", 123);
		
		List<Employee> empList = new LinkedList<Employee>();
		
		empList.add(emp1);
		
		//listNames.stream().sorted().forEach(names -> System.out.println(names));
		
		//listNames.stream().distinct().sorted().forEach(names -> System.out.println(names));
		
		//listNames.stream().filter(x -> x.startsWith("A")).forEach(names -> System.out.println(names));
		
		//List<String> fltData = 
		//		listNames.stream().filter(x -> x.startsWith("b")).collect(Collectors.toList());
		
		//fltData.forEach(x -> System.out.println(x));
		
		//listNames.parallelStream().filter(x -> x.contains("a")).forEach(names -> System.out.println(names));
		
		//listNames.forEach(System.out::println);
		
		//listNames.stream().anyMatch(x -> x.startsWith("a"));
		
		Map<String, Long> data = 
				listNames.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		
		System.out.println(data);
		
		Map<Integer, String> mData = 
				
		empList.stream().collect(Collectors.toMap(Employee::getId, Employee::getName));
		
		mData.forEach((k, v)->System.out.println(k+" "+v));
		
		//List<String> lData = mData.entrySet().stream().map().collect(Collectors.toList());
		
		List<String> lData = mData.values().stream().collect(Collectors.toList());
		
		List<Integer> lData1 = mData.keySet().stream().collect(Collectors.toList());
	}

}







