package com.test1;

public class LifeCycle extends Thread
{
	@Override
	public void run()
	{
		try {
			Thread.sleep(2000);
			System.out.println("run()");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		
		LifeCycle t1 = new LifeCycle();
		
		System.out.println("Before starting thread status : "+t1.isAlive());
		System.out.println("Before starting thread state : "+t1.getState());
		
		t1.start();
		
		System.out.println("after starting thread status : "+t1.isAlive());
		System.out.println("after starting thread state : "+t1.getState());
		
		Thread.sleep(1000);
		
		System.out.println("in sleep thread status : "+t1.isAlive());
		System.out.println("ins sleep thread state : "+t1.getState());
		
		t1.join();
		
		System.out.println("after join thread state : "+t1.getState());
		System.out.println("after join sthread status : "+t1.isAlive());
	}
}
