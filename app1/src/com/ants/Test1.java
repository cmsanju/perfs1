package com.ants;

import java.util.Arrays;
import java.util.function.Function;

class Recursive<I>
{
	public I func;
}

public class Test1 {
  public static void main(String[] args) {
	
	  Recursive<Function<Integer, Integer>> rs = new Recursive<>();
	  
	  rs.func = (n)->n<=1?1:n*rs.func.apply(n-1);
	  
	  int g = rs.func.apply(4);
	  
	  System.out.println(g);
	  
	  //java
}
}
