package com.voter;

import java.util.Scanner;

public class Test {
	
	public static void main(String[] args) throws ValidAgeException,InvalidAgeException
	{
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter your age");
		
		int age = sc.nextInt();
		
		Validator v = new Validator();
		
		v.checkAge(age);
		
		/*
		try {
			
			v.checkAge(age);
		}
		catch(ValidAgeException ve)
		{
			System.out.println("valid age");
		}
		catch(InvalidAgeException ie)
		{
			System.out.println("invalid age");
		}
		*/
	}
}
