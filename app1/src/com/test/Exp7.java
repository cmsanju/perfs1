package com.test;

class Student1
{
	private int id;
	private String name;
	private String city;
	
	public Student1()
	{
		
	}
	
	public Student1(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		Student1[] data = new Student1[5];
		
		Student1 std1 = new Student1(101, "Hero", "Blr");
		Student1 std2 = new Student1(102, "Java", "Hyd");
		Student1 std3 = new Student1(103, "MAC", "Chn");
		Student1 std4 = new Student1(104, "app", "Pune");
		Student1 std5 = new Student1(105, "developer", "Kolkata");
		
		data[0] = std1;
		data[1] = std2;
		data[2] = std3;
		data[3] = std4;
		data[4] = std5;
		
		for(Student1 obj : data)
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" City : "+obj.getCity());
		}
				
		
	}

}
