package com.test;

public class Exp9 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		
		//str1.append();
		
		StringBuffer sb = new StringBuffer(str1);
		
		sb.append(" Hello");
		
		System.out.println(sb);
		
		System.out.println(sb.reverse());
		
		String str3 = "Java";
		String str4 = "java";
		
		System.out.println(str3.equalsIgnoreCase(str4));
		
		String str5 = str3.substring(2);
		
		System.out.println(str5);
		
		String str6 = "Simple java";
		
		String str7 = str6.substring(2, str6.length());
		
		System.out.println(str7);
	}

}
