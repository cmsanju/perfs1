package com.test;

public class Exp6 {
		
	public static void main(String[] args) {
		
		int[] ar = {10,20,30,40};
		
		System.out.println(ar.length);
		
		System.out.println(ar[0]);
		
		
		for(int x : ar)
		{
			System.out.println(x);
		}
		
		//System.out.println(ar[5]);
		
		String[] names = new String[5];
		
		names[4] = "Spring";
		
		System.out.println(names.length);
		
		System.out.println(names[0].equals("java"));
		
		
		
	}
}
