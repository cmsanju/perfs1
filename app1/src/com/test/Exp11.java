package com.test;

//count no of words in string

public class Exp11 {
	
private static void countNumberOfWords(String line) {
		
		String trimmedLine = line.trim();
		int count = trimmedLine.isEmpty() ? 0 : trimmedLine.split("\\s+").length;
		
		System.out.println(count);
	}
	
public static void main(String[] args) {
		
		countNumberOfWords("Hi this is simple java code");
		countNumberOfWords("Strings in java");
		countNumberOfWords("  Java test and   development  ");

	}

	

}
