package com.test;

//How to remove all occurrences of a given character from input String?

public class Exp12 {
	
	private static void removeCharFromString(String input, char c) {
		String result = input.replaceAll(String.valueOf(c), "");
		System.out.println(result);
	}
	
	public static void main(String[] args) {

		removeCharFromString("java", 'a');
		removeCharFromString("Spring", 'n');
		removeCharFromString("Developer", 'e');

	}

	

}
