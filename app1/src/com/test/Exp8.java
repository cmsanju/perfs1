package com.test;

public class Exp8 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		String str2 = "java";
		String str3 = "Hello";
		String str4 = "hello";
		
		String str5 = new String("java");
		String str6 = new String("hello");
		
		
		System.out.println(str3 == str4);
		
		System.out.println(str1 == str5);
		
		System.out.println(str4 == str6);
		
		System.out.println(str1.equals(str5));
		
		System.out.println(str1.length());
		
		System.out.println(str1.charAt(0));
		
		
		for(int i = str1.length()-1; i >= 0; i--)
		{
			System.out.print(str1.charAt(i)+" ");
		}
		
		System.out.println();
		
		str1.concat(" test");
		
		System.out.println(str1);
	}
}






