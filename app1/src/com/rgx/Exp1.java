package com.rgx;

import java.util.Scanner;

import com.ants.CutomA;

@CutomA
public class Exp1 {
	 public static void main(String[] args) {
	      Scanner sc = new Scanner(System.in);
	      
	      System.out.println("enter your password");
	      
	      String pass = sc.next();
	      
	      String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
	     
	      System.out.println(pass.matches(pattern));
	   }
}

/*

Explanations:

(?=.*[0-9]) a digit must occur at least once
(?=.*[a-z]) a lower case letter must occur at least once
(?=.*[A-Z]) an upper case letter must occur at least once
(?=.*[@#$%^&+=]) a special character must occur at least once
(?=\\S+$) no whitespace allowed in the entire string
.{8,} at least 8 characters

*/






