package com.rgx;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exp2 {
	
	public static void main(String[] args) {
		
		System.out.println(Pattern.matches(".p", "ap"));
		
		Pattern ptr = Pattern.compile("[^abc]");
		
		String input = "x";
		
		boolean b = Pattern.matches("[^abc]" , input);
		
		System.out.println(b);
	}

}
