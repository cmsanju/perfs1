package com.cls;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Exp4 {
	
	public static void main(String[] args) {
		
		Integer[] ar = {12,56,78,11,2,34,53,8,5};
		
		List<Integer> data = Arrays.asList(ar);
		
		data.add(100);
		
		System.out.println("before sorting");
		
		System.out.println(data);
		
		Collections.sort(data);
		
		System.out.println("after sorting");
		
		System.out.println(data);
		
		System.out.println(data.get(data.size()-1));
		
		Collections.shuffle(data);
		
		System.out.println(data);
	}

}
