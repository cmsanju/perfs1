package com.cls;

import java.util.Stack;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add("java");
		data.add('A');
		data.add(true);
		data.add(34.44);
		data.add(33.73f);
		data.add(10);
		data.add("java");
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		data.push("hello");
		
		System.out.println(data);
		
		System.out.println(data.pop());
		
		System.out.println(data.search(100));
		
		data.clear();
		
		System.out.println(data.empty());
	}

}
