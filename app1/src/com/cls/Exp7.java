package com.cls;

import java.util.HashSet;
import java.util.Set;

public class Exp7 {
	
	public static void main(String[] args) {
		
		Set<Employee> empData = new HashSet<Employee>();
		
		empData.add(new Employee(101, "Dilip", 22, "perfs", 345.33));
		empData.add(new Employee(101, "Dilip", 22, "perfs", 345.33));
		empData.add(new Employee(102, "Aditya", 24, "PWC", 463.38));
		empData.add(new Employee(103, "Balaji", 21, "E Y", 338.33));
		empData.add(new Employee(104, "Umar", 25, "Dell", 234.44));
		empData.add(new Employee(105, "Devesh", 27, "JW", 123.44));
		empData.add(new Employee(105, "Devesh", 27, "JW", 123.44));
		
		System.out.println(empData.size());
	}
}
