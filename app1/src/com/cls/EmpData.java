package com.cls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

class NameComparator implements Comparator<Employee>
{

	@Override
	public int compare(Employee o1, Employee o2) {
		
		return o2.getName().compareTo(o1.getName());
	}
	
}

public class EmpData {
	
	public static void main(String[] args) {
		
		List<Employee> empData = new ArrayList<Employee>();
		
		empData.add(new Employee(101, "Dilip", 22, "perfs", 345.33));
		empData.add(new Employee(102, "Aditya", 24, "PWC", 463.38));
		empData.add(new Employee(103, "Balaji", 21, "E Y", 338.33));
		empData.add(new Employee(104, "Umar", 25, "Dell", 234.44));
		empData.add(new Employee(105, "Devesh", 27, "JW", 123.44));
		empData.add(new Employee(105, "Devesh", 27, "JW", 123.44));
		
		Iterator<Employee> itr = empData.iterator();
		
		while(itr.hasNext())
		{
			Employee emp = itr.next();
			
			System.out.println("ID : "+emp.getId()+" Name : "+emp.getName()+" Age : "+emp.getAge()+" "
					+ " Company : "+emp.getCmp()+" Salary : "+emp.getSalary());
		}
		
		Collections.sort(empData, new NameComparator());
		
		Iterator<Employee> itr1 = empData.iterator();
		
		while(itr1.hasNext())
		{
			Employee emp = itr1.next();
			
			System.out.println("ID : "+emp.getId()+" Name : "+emp.getName()+" Age : "+emp.getAge()+" "
					+ " Company : "+emp.getCmp()+" Salary : "+emp.getSalary());
		}
	}

}
