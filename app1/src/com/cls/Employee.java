package com.cls;

import java.util.Objects;

public class Employee implements Comparable<Employee>
{
	
	private int id;
	
	private String name;
	
	private int age;
	
	private String cmp;
	
	private double salary;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, int age, String cmp, double salary) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.cmp = cmp;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCmp() {
		return cmp;
	}
	public void setCmp(String cmp) {
		this.cmp = cmp;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public int compareTo(Employee o) {
		
		return this.age - o.getAge();
	}

	@Override
	public int hashCode() {
		return Objects.hash(age, cmp, id, name, salary);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		return age == other.age && Objects.equals(cmp, other.cmp) && id == other.id && Objects.equals(name, other.name)
				&& Double.doubleToLongBits(salary) == Double.doubleToLongBits(other.salary);
	}

}
