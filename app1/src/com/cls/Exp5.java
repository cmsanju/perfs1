package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Exp5 {
	
	public static void main(String[] args) {
		
		//Set<String> data = new HashSet<>();
		
		HashSet<String> data = new HashSet<>();
		
		data.add("java");
		data.add("php");
		data.add("apple");
		data.add("lenovo");
		data.add("sony");
		data.add("java");
		data.add("sony");
		
		System.out.println(data);
		
		LinkedHashSet<String> data1 = new LinkedHashSet<>();
		
		data1.add("java");
		data1.add("php");
		data1.add("apple");
		data1.add("lenovo");
		data1.add("sony");
		data1.add("java");
		data1.add("sony");
		
		System.out.println(data1);
		
		System.out.println(data.size());
		
		System.out.println(data.remove("java"));
		
		Iterator<String> itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
