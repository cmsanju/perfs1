package com.cls;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Exp2 {
	
	public static void main(String[] args) {
		
		//Collection data = new LinkedList();
		//List data = new LinkedList();
		
		List data = new LinkedList();
		
		data.add(10);
		data.add("java");
		data.add('A');
		data.add(true);
		data.add(34.44);
		data.add(33.73f);
		data.add(10);
		data.add("java");
		
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		System.out.println(data.contains(10));
		
		data.set(3,"java");
		
		System.out.println(data.get(3));
		
		System.out.println(data.indexOf(10));
		
		System.out.println(data);
		
		data.remove(5);
		
		System.out.println(data);
		
		
		//Iterator, ListIterator and Enumeration
		
		//Iterator itr = data.iterator();
		
		ListIterator ltr = data.listIterator();
		
		while(ltr.hasNext())
		{
			System.out.println(ltr.next());
		}
		
		while(ltr.hasPrevious())
		{
			System.out.println(ltr.previous());
		}
		
		
	}

}
