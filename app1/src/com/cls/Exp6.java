package com.cls;

import java.util.Iterator;
import java.util.TreeSet;

public class Exp6 {
	
	public static void main(String[] args) {
		
		TreeSet<String> data = new TreeSet<String>();
		
		data.add("java");
		data.add("php");
		data.add("apple");
		data.add("lenovo");
		data.add("sony");
		data.add("java");
		data.add("sony");
		
		System.out.println(data.ceiling("php"));
		System.out.println(data.subSet("apple", "sony"));
		
		Iterator<String> itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.print(itr.next()+" ");
		}
		
		Iterator<String> itr1 = data.descendingIterator();
		System.out.println();
		while(itr1.hasNext())
		{
			System.out.print(itr1.next()+" ");
		}
		
		System.out.println();
		System.out.println(data.tailSet("lenovo"));
	}
}













