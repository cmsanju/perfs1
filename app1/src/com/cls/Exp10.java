package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp10 {
	
	public static void main(String[] args) {
		
		Employee emp1 = new Employee(101, "rohit", 21, "hcl", 234);
		Employee emp2 = new Employee(102, "devesh", 11, "hcl", 34);
		Employee emp3 = new Employee(103, "umar", 15, "hcl", 22);
		Employee emp4 = new Employee(104, "balaji", 23, "hcl", 55);
		Employee emp5 = new Employee(105, "adhithya", 25, "hcl", 11);
		
		
		//Map<String, Employee> empData = new HashMap<>();
		Map<String, Employee> empData = new TreeMap<>();
		
		empData.put(emp1.getName(), emp1);
		empData.put(emp2.getName(), emp2);
		empData.put(emp3.getName(), emp3);
		empData.put(emp4.getName(), emp4);
		empData.put(emp5.getName(), emp5);
		
		Iterator<Entry<String, Employee>> itr = empData.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Employee> et = itr.next();
			
			System.out.println("Name : "+et.getKey()+" "
					+ "Details : "+et.getValue().getId()+" "+et.getValue().getName()+" "
					+et.getValue().getAge()+" "+et.getValue().getCmp()+" "+et.getValue().getSalary());
		}
		
	}
}






