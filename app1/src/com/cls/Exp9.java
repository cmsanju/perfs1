package com.cls;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Exp9 {
	
	public static void main(String[] args) {
		
		LinkedHashMap<String, Integer> data = new LinkedHashMap<>();
		
		data.put("lenovo", 100);
		data.put("sony", 350);
		data.put("mac", 450);
		data.put("dell", 200);
		data.put("apple", 455);
		data.put("asus", 100);
		data.put("sony", 400);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Product : "+et.getKey()+" Price : "+et.getValue());;
			
		}
		
		for(String k : data.keySet())
		{
			System.out.println("Key : "+k+" Value : "+data.get(k));
		}
		
		for(Integer v : data.values())
		{
			System.out.println("Values : "+v);
		}
		
		//java 8 new feature
		
		data.forEach((k,v) -> System.out.println(k+" "+v));
	}

}











