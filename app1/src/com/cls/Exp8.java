package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Exp8 {
	
	public static void main(String[] args) {
		
		Map data = new HashMap();
		
		data.put(12, "java");
		data.put("id", 101);
		data.put('A', "grade");
		data.put("id", 102);
		data.put(300, 100);
		data.put("name", "java");
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		System.out.println(data.get("id"));
		
		Iterator<Entry> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry et = itr.next();
			
			System.out.println(et.getKey()+" : "+et.getValue());
		}
		
		//java 8 new feature
		data.forEach((k,v)-> System.out.println(k+" "+v));
	}

}






