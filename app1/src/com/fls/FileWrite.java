package com.fls;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class FileWrite 
{
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/sample.txt");
		
		FileOutputStream fout = new FileOutputStream(file);
		
		BufferedOutputStream bw = new BufferedOutputStream(fout);
		
		String msg = "This is simple byte stream write operation";
		
		bw.write(msg.getBytes());
		
		bw.flush();
		
		System.out.println("Done.");
	}
}





