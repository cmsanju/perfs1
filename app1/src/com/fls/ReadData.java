package com.fls;

import java.io.BufferedReader;
import java.io.FileReader;

public class ReadData {
	
	public static void main(String[] args) throws Exception
	{
		
		FileReader fw = new FileReader("src/write.txt");
		
		BufferedReader bw = new BufferedReader(fw);
		
		System.out.println(bw.readLine()+"\n");
		
		char[] cr = new char[1024];
		
		int x = 0;
		
		while( (x = bw.read(cr)) != -1 )
		{
			System.out.println(new String(cr,0,x));
		}
	}
}








