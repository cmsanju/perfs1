package com.fls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/player.txt");
		
		FileOutputStream fos = new FileOutputStream(file);
		
		ObjectOutputStream ow = new ObjectOutputStream(fos);
		
		Player obj = new Player();
		
		obj.id = 101;
		obj.name = "PlayerName";
		obj.type = "PlayerType";
		obj.city = "PlayerCity";
		obj.pin = 123123;
		
		ow.writeObject(obj);
		
		System.out.println("Done.");
	}

}




