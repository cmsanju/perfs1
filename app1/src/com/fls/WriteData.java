package com.fls;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class WriteData {
	
	public static void main(String[] args) throws Exception
	{
		//File file = new File("src/write.txt");
		
		FileWriter fw = new FileWriter("src/write.txt",true);
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		String msg = "\n This is simple char stream write operation";
		
		bw.write(msg);
		
		bw.flush();
		
		System.out.println("Done.");
	}

}
