package com.excp;

import java.util.Scanner;

public class Exp1 {
	
	public static void main(String[] args) {
		Scanner sc = null;
		try {
			sc = new Scanner(System.in);
			
	//sc.next();
			
			System.out.println("Test");
		
			System.out.println(100/0);
		
			System.out.println("Exceptions");
		
			//args[0] = null;
			String str = "java";
			System.out.println(str.equals("test"));
		
			int x = Integer.parseInt("o");
			
			System.exit(0);
		}
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero.");
			System.exit(0);
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check your array size");
		}
		catch(NullPointerException npe)
		{
			System.out.println("provide string inputs");
		}
		catch (NumberFormatException nfe) {
			
			System.out.println("enter only numerics");
		}
		catch (Exception e) 
		{
			System.out.println("check your inputs");
		}
		finally
		{
			try {
				
				System.out.println("finally block");
				
				if(sc == null)
				{
					sc.close();
				}
				
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		
	}

}
