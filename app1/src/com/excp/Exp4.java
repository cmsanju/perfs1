package com.excp;

public class Exp4 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(200/0);
		}
		catch(Exception e)
		{
			//using getMessage(); it will give only exception message
			System.out.println(e.getMessage());
			
			//printing exception class object 
			//it will provide exception class name and message
			System.out.println(e);
			
			//using printStackTrace(); 
			//it will give exception class name
			//exception message 
			//line number
			
			e.printStackTrace();
		}
	}

}
