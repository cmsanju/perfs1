package com.newftrs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Exp1 {
	
	public static void main(String[] args) {
		
		List<String> data = new ArrayList<String>();
		
		data.add("java");
		data.add("java");
		data.add("python");
		data.add("kotlin");
		data.add("groovy");
		data.add("spring");
		data.add("spring");
		data.add("hibernate");
		data.add("microservices");
		
		//java 8 features
		
		//data.forEach(x -> System.out.println(x));
		/*
		data.stream().distinct().forEach(x -> System.out.println(x));
		
		List<String> unq = data.stream().distinct().collect(Collectors.toList());
		
		System.out.println(unq);
		
		System.out.println(data.stream().anyMatch(x -> x.endsWith("groovy")));
		
		List<String> dataf = data.stream().filter(x -> x.startsWith("j")).collect(Collectors.toList());
		
		System.out.println(dataf);
		*/
		
		data.stream().sorted().forEach(x -> System.out.println(x));
	}
}
