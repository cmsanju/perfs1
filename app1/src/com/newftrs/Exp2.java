package com.newftrs;

@FunctionalInterface
interface FunInf
{
	String message(String str);
}


public class Exp2 {
	
	public static void main(String[] args) {
		FunInf obj = (String str) -> {
			
			System.out.println("Overrided : "+str);
			
			return str;
		};
		
		obj.message("hello");
	}
}
