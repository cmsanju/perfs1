package com.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigExp {
	
	@Bean
	public Student getObj1()
	{
		return new Student();
	}
	
	@Bean
	public Student getObj2()
	{
		return new Student();
	}
	
	@Bean
	public Address getObj3()
	{
		return new Address();
	}
}
