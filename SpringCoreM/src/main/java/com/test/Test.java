package com.test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Test {
	
	public static void main(String[] args) {
		
		//Resource rs = new ClassPathResource("beans.xml");
		
		//BeanFactory bn = new XmlBeanFactory(rs);
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		Student s = ctx.getBean("std", Student.class);
		
		s.disp();
		
		//Student s1 = ctx.getBean("std1", Student.class);
		
		//s1.disp();
		
		/*
		ApplicationContext ctx = new AnnotationConfigApplicationContext(ConfigExp.class);
		
		Address obj1 = ctx.getBean("getObj3",Address.class);
		
		obj1.setState("KA");
		obj1.setCnt("Bharat");
		obj1.setPin(123123);
		
		Student obj2 = ctx.getBean("getObj2", Student.class);
		
		obj2.setAdr(obj1);
		obj2.setId(101);
		obj2.setName("Nitesh");
		obj2.setCity("BTM");
		
		
		obj2.disp();
		*/
		
	}
}
