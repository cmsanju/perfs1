package com.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class Student {
	
	private int id;
	
	private String name;
	
	private String city;
	
	private List<String> skills;
	
	@Autowired
	private Address adr;
	
	public Student()
	{
		
	}
	
	public Student(int id, String name, String city, Address adr)
	{	
		this.id = id;
		this.name = name;
		this.city = city;
		this.adr = adr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public Address getAdr() {
		return adr;
	}

	public void setAdr(Address adr) {
		this.adr = adr;
	}
	
	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" City : "+city);
		System.out.println("Skill");
		
		for(String s : skills)
		{
			System.out.println(s);
		}
		
		adr.details();
	}
}
