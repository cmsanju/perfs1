package com.test;

public class Address {
	
	private String state;
	
	private String cnt;
	
	private int pin;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCnt() {
		return cnt;
	}

	public void setCnt(String cnt) {
		this.cnt = cnt;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}
	
	public void details()
	{
		System.out.println("State : "+state+" Country : "+cnt+" Pincode : "+pin);
	}
}
