package com.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction tm = session.beginTransaction();
		
		Employee emp = new Employee();
		
		emp.setEmp_name("Rakesh");
		emp.setEmp_cmp("IBM");
		
		session.save(emp);
		
		Employee emp1 = session.get(Employee.class, 1);
		
		System.out.println(emp1.getId()+" "+emp1.getEmp_name()+" "+emp1.getEmp_cmp());
		
		tm.commit();
		
		System.out.println("Done.");
		
		session.close();
	}
}
