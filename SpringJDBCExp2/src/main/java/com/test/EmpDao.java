package com.test;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.aop.ThrowsAdvice;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

public class EmpDao {
	
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int save(Employee emp)
	{
		String sql = "insert into employee1 values("+emp.getId()+",'"+emp.getEmp_name()+"','"+emp.getEmp_cmp()+"')";
		
		return jdbcTemplate.update(sql);
	}
	
	public int update(Employee emp)
	{
		String sql = "update employee1 set emp_name='"+emp.getEmp_name()+"', emp_cmp='"+emp.getEmp_cmp()+"' where id="+emp.getId()+" ";
		
		return jdbcTemplate.update(sql);
	}
	
	public int delete(Employee emp)
	{
		String sql = "delete from employee1 where id="+emp.getId();
		
		return jdbcTemplate.update(sql);
	}
	/*
	public List<Employee> getAll()
	{
		return jdbcTemplate.query("select * from employee1", new ResultSetExtractor<List<Employee>>() {
			
			public List<Employee> extractData(ResultSet rs)throws SQLException,DataAccessException
			{
				List<Employee> empList = new ArrayList<Employee>();
				
				while(rs.next())
				{
					Employee emp = new Employee();
					emp.setId(rs.getInt(1));
					emp.setEmp_name(rs.getString(2));
					emp.setEmp_cmp(rs.getString(3));
					
					empList.add(emp);
				}
				return empList;
			}
		});
	}
	
	*/
	
	public List<Employee> getAll()
	{
		return jdbcTemplate.query("select * from employee1", new RowMapper<Employee>() {
			public Employee mapRow(ResultSet rs, int row)throws SQLException
			{
				Employee emp = new Employee();
				
				emp.setId(rs.getInt(1));
				emp.setEmp_name(rs.getString(2));
				emp.setEmp_cmp(rs.getString(3));
				
				return emp;
			}
		});
	}
}
