package com.test;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		EmpDao obj = (EmpDao)ctx.getBean("edao");
		
		Employee emp = new Employee();
		
		emp.setId(201);
		emp.setEmp_name("Vishnu");
		emp.setEmp_cmp("Mathura");
		
		//obj.save(emp);
		
		//obj.update(emp);
		
		//obj.delete(emp);
		
		List<Employee> data = obj.getAll();
		
		for(Employee e : data)
		{
			System.out.println(e.getId()+" "+e.getEmp_name()+" "+e.getEmp_cmp() );
		}
		
		System.out.println("done.");
	}
}	
