package com.cls;

import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add("java");
		data.add(10);
		data.add("java");
		data.add(23.33);
		data.add(99.33f);
		data.add('A');
		data.add(false);
		
		System.out.println(data.peek());
		
		data.push("sudheer");
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		System.out.println(data.peek());
		
		System.out.println(data.search(100));
		
		System.out.println(data.empty());
		
		data.clear();
		
		System.out.println(data.empty());
	}

}
