package com.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//List data = new ArrayList();
		//ArrayList data = new ArrayList();
		LinkedList data = new LinkedList();
		
		data.add(10);
		data.add("java");
		data.add(10);
		data.add("java");
		data.add(23.33);
		data.add(99.33f);
		data.add('A');
		data.add(false);
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		System.out.println(data.get(5));
		
		data.set(5, "sudheer");
		
		System.out.println(data.get(5));
		
		data.remove(5);
		
		System.out.println(data.size());
		
		//cursors 3 types
		//1 Iterator 2 ListIterator 3 Enumeration
		
		//Iterator itr = data.iterator();
		
		ListIterator ltr = data.listIterator();
		
		while(ltr.hasNext())
		{
			System.out.println(ltr.next());
		}
		
		System.out.println("======");
		
		while(ltr.hasPrevious())
		{
			System.out.println(ltr.previous());
		}
	}
}
/*
CREATE
READ
UPDATE
DELETE

*/