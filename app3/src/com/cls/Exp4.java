package com.cls;

import java.util.Iterator;
import java.util.TreeSet;

public class Exp4 {
	
	public static void main(String[] args) {
		
		TreeSet<String> data = new TreeSet<>();
		
		data.add("hello");
		data.add("java");
		data.add("php");
		data.add("java");
		data.add("spring");
		data.add("hibernate");
		data.add("framework");
		data.add("collection");
		
		System.out.println(data);
		
		Iterator<String> itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
