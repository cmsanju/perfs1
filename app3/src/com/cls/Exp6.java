package com.cls;

import java.util.TreeMap;

public class Exp6 {
	
	public static void main(String[] args) {
		
		TreeMap<String, Integer> data = new TreeMap<String, Integer>();
		
		data.put("lenovo", 1212);
		data.put("dell", 2332);
		data.put("hcl", 3299);
		data.put("asus", 838);
		data.put("sony", 3000);
		data.put("dell", 3939);
		data.put("apple", 4445);
		
		System.out.println(data);
	}
}
