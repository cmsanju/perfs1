package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Exp5 {
	
	public static void main(String[] args) {
		
		Map<String, Integer> data = new HashMap<>();
		
		data.put("lenovo", 1212);
		data.put("dell", 2332);
		data.put("hcl", 3299);
		data.put("asus", 838);
		data.put("sony", 3000);
		data.put("dell", 3939);
		data.put("apple", 4445);
		
		System.out.println(data);
		
		LinkedHashMap<String, Integer> data1 = new LinkedHashMap<String, Integer>();
		
		data1.put("lenovo", 1212);
		data1.put("dell", 2332);
		data1.put("hcl", 3299);
		data1.put("asus", 838);
		data1.put("sony", 3000);
		data1.put("dell", 3939);
		data1.put("apple", 4445);
		
		System.out.println(data1);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println(et.getKey()+" : "+et.getValue());
		}
		
		for(String k : data.keySet())
		{
			System.out.println(k+" "+data.get(k));
		}
		
		for(Integer x : data.values())
		{
			System.out.println("Values : "+x);
		}
		
		//java 8
		
		data.forEach((k,v)-> System.out.println(k+" "+v));
		
		System.out.println(data.hashCode());
	}
}
//array of bucket (node<k,v>) hashCode(), equals()
