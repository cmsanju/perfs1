package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(10/0);
		}
		catch (Exception e) {
			//1 using getMessage() method only displays exception message
			System.out.println(e.getMessage());
			
			//2 printing exception class object
			System.out.println(e);
			
			//3 using printStackTrace() method 
			e.printStackTrace();
		}
	}
}
