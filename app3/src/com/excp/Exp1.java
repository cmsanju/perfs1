package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		
		try
		{

			System.out.println("Hello");
			
			System.out.println(100/2);
			
			Class.forName("com.excp.Exp1");
			
			int[] ar = {10,20,30,40};
			
			System.out.println(ar[7]);
			
			String str = "sudheer";
			
			System.out.println(str.charAt(3));
			
			System.out.println("Test");
		}
		//System.out.println();
		
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(ClassNotFoundException cnf)
		{
			System.out.println("give correct class name");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check array size");
		}
		catch (StringIndexOutOfBoundsException sie) {
			
			System.out.println("check your string length");
		}
		catch(NullPointerException npe)
		{
			System.out.println("enter string input");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		//System.out.println();
		finally
		{
			System.out.println("i am from finally");
		}
		
	}
}
