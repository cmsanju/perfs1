package com.ths;

public class Exp3 implements Runnable
{
	public void run()
	{
		System.out.println("run() : "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args)throws Exception
	{
		
		Exp3 t1 = new Exp3();
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		//Convert Runnable interface object into thread class
		Thread t2 = new Thread(tg1, t1, "Transfer");
		
		Thread t3 = new Thread(tg1, t1, "Withdraw");
		
		Thread t4 = new Thread(tg1, t1, "Credit");
		
		ThreadGroup tg2 = new ThreadGroup("Arithmetic");
		
		Thread t5 = new Thread(tg2, t1, "Add");
		
		Thread t6 = new Thread(tg2, t1, "Sub");
		
		Thread t7 = new Thread(tg2, t1, "Div");
		
		t2.start();
		t7.start();
		
		System.out.println("Tg1 : "+tg1.activeCount());
		System.out.println("Tg2 : "+tg2.activeCount());
		
		t2.join();
		
		t7.join();
		
		tg1.destroy();
		tg2.destroy();
		System.out.println(tg1.isDestroyed());
		System.out.println(tg2.isDestroyed());
	}
}

/*
 * synchronized block
 * synchronized non-static / instance methods : object level data
 * synchronized static methods : class level data 
 * 
 */
