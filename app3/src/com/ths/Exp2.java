package com.ths;

public class Exp2 extends Thread
{
	
	public void run()
	{
		System.out.println("i am from run()");
	}
	
	public static void main(String[] args) {
		
		System.out.println(MAX_PRIORITY);
		System.out.println(NORM_PRIORITY);
		System.out.println(MIN_PRIORITY);
		
		Exp2 t1 = new Exp2();
		Exp2 t2 = new Exp2();
		Exp2 t3 = new Exp2();
		Exp2 t4 = new Exp2();
		
		System.out.println("default thread priority : "+t1.getPriority());
		System.out.println("default thread priority : "+t2.getPriority());
		System.out.println("default thread priority : "+t3.getPriority());
		System.out.println("default thread priority : "+t4.getPriority());
		
		System.out.println("default thread name : "+t1.getName());
		System.out.println("default thread name : "+t2.getName());
		System.out.println("default thread name : "+t3.getName());
		System.out.println("default thread name : "+t4.getName());
		
		t1.setPriority(MAX_PRIORITY);
		t3.setPriority(MIN_PRIORITY);
		System.out.println("after setting thread priority : "+t1.getPriority());
		System.out.println("after setting thread priority : "+t3.getPriority());
		t1.setName("add");
		t2.setName("sub");
		t3.setName("div");
		t4.setName("mul");
		
		System.out.println("after setting thread name : "+t1.getName());
		System.out.println("after setting thread name : "+t2.getName());
		System.out.println("after setting thread name : "+t3.getName());
		System.out.println("after setting thread name : "+t4.getName());
	}
}
