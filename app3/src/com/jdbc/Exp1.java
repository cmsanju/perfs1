package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		//2 create connection object
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jandev", "root", "password");
		
		//3 create statement object
		
		Statement stmt = con.createStatement();
		
		//String sql = "create table emp1(id int, emp_name varchar(50))";
		
		String sql1 = "insert into emp1 values(107, 'Ranjan')";
		String sql2 = "insert into emp1 values(104, 'Ranjan')";
		String sql3 = "insert into emp1 values(105, 'Ranjan')";
		String sql4 = "insert into emp1 values(106, 'Ranjan')";
		
		//4 execute query
		
		stmt.execute(sql1);
		
		stmt.addBatch(sql2);
		stmt.addBatch(sql3);
		stmt.addBatch(sql4);
		
		stmt.executeBatch();
		
		String sql = "select * from emp1";
		
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()) {
			
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2));
		}
		
		//5 close the connection
		con.close();
	}
}
