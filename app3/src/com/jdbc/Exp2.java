package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class Exp2 {
	public static void main(String[] args) 
	{
		try {
		
		//1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		//2 create connection object
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jandev", "root", "password");
		
		con.setAutoCommit(false);
		
		//3 create statement object
		
		PreparedStatement pst3 = con.prepareStatement("insert into emp1 values(?,?)");
		
		pst3.setInt(1, 201);
		pst3.setString(2, "Gupta");
		//4 execute the query
		pst3.execute();
		//con.commit();
		con.rollback();
		/*
		PreparedStatement pst2 = con.prepareStatement("update emp1 set emp_name = ? where id = ?");
		
		pst2.setString(1, "Shivam");
		pst2.setInt(2, 201);
		
		pst2.execute();
		
		
		
		PreparedStatement pst = con.prepareStatement("delete from emp1 where id = ?");
		
		pst.setInt(1, 201);
		
		pst.execute();
		*/
		PreparedStatement pst1 = con.prepareStatement("select * from emp1");
		
		pst1.addBatch();
		
		ResultSet rs = pst1.executeQuery();
		
		while(rs.next()) {
		
		System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2));
		}
		
		ResultSetMetaData rsd = rs.getMetaData();
		
		System.out.println(rsd.getColumnCount());
		System.out.println(rsd.getColumnTypeName(1)+" "+rsd.getColumnName(1));
		
		//5 close the connection
		
		con.close();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
