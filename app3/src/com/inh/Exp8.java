package com.inh;

public class Exp8 {
	
	public static void main(String[] args) {
		
		int x = 200;
		
		double y = 400;
		
		String v1 = String.valueOf(x);
		String v2 = String.valueOf(y);
		
		//by default super or parent class in java Object class
		
		//auto-boxing
		
		long l = 400;
		
		Long ll = new Long(l);
		
		//auto un-boxing
		
		Double d = new Double(300);
		
		double dd = d;
	}
}
