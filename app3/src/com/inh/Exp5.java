package com.inh;

interface Inf1
{
	void cat();
}

interface Inf2
{
	void dog();
}

interface Inf3 //extends Inf1, Inf2
{
	void fox();
}

class Impl1 implements Inf1, Inf2,Inf3
{
	@Override
	public void cat()
	{
		
	}
	@Override
	public void dog()
	{
		
	}
	@Override
	public void fox()
	{
		
	}
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		
		
	}
}
