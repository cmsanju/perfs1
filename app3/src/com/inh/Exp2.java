package com.inh;

public class Exp2 {
	
	public void add(int x)
	{
		System.out.println("single");
	}
	public void add(String x)
	{
		System.out.println("type of the args");
	}
	public void add(int x, double y)
	{
		System.out.println("no of the args");
	}
	public void add(double x, int y)
	{
		System.out.println("order of the args");
	}
	
	public static void main(String[] args) {
		
		Exp2 e = new Exp2();
		
		e.add(10);
		e.add("hello");
		e.add(30, 38.33);
		e.add(20.22,11);
	}

}
