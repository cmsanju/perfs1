package com.inh;

class H
{
	public void message()
	{
		System.out.println("WA");
	}
}

class I extends H
{
	@Override
	public void message()
	{
		System.out.println("email");
	}
}

class J extends H
{
	@Override
	public void message()
	{
		System.out.println("text");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		H obj1 = new I();
		
		obj1.message();
		
		H obj2 = new J();
		
		obj2.message();
	}
}
