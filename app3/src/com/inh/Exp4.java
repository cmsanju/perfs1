package com.inh;

interface Inf
{
	void human();
	
	//java 8 version
	
	default void book()
	{
		System.out.println("default");
	}
	
	static void color()
	{
		System.out.println("static");
	}
}

abstract class Abs //implements Inf
{
	public void show()
	{
		System.out.println("show() abs");
	}
	
	public abstract void cat();
	
}

class Impl extends Abs implements Inf
{
	public void disp()
	{
		System.out.println("disp");
	}
	
	@Override
	public void human()
	{
		System.out.println("inf");
	}
	
	@Override
	public void cat()
	{
		System.out.println("cat");
	}
	
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		Impl obj = new Impl();
		
		obj.show();
		obj.cat();
		obj.disp();
		obj.human();
		obj.book();
		
		Inf.color();
	}

}
