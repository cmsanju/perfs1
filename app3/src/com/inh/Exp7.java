package com.inh;

@FunctionalInterface
interface FunInf
{
	void greet();
	
	default void disp()
	{
		
	}
	
	static void cat()
	{
		
	}
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		FunInf obj1 = new FunInf()
				{
					public void greet()
					{
						System.out.println("object crated");
					}
				};
		obj1.greet();
		
		new FunInf()
		{
			public void greet()
			{
				System.out.println("nameless object");
			}
		}.greet();
		
		//jdk 8
		
		FunInf obj2 = ()-> {
			System.out.println("lambda expression");
			
			//return 
		};
		
		obj2.greet();
	}
}
