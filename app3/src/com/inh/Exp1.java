package com.inh;

class A
{
	  int id = 101;
	String name = "Java";
	
	public void disp()
	{
		System.out.println(id+" "+name);
	}
}

class B extends A
{
	String city = "TPT";
	
	public void show()
	{
		System.out.println(id+" "+name+" "+city);
	}
	
	@Override
	public void disp()
	{
		
	}
}

class C extends B
{
	String state = "AP";
	
	public void details()
	{
		System.out.println(id+" "+name+" "+city+" "+state);
	}
}

public class Exp1 {
	
	public static void main(String[] args) {
		
		//B b = new B();
		
		//b.disp();
		//b.show();
		
		C c = new C();
		
		c.details();
	}
}
