package com.test;

class Exp3
{
	public void show()
	{
		System.out.println("test");
	}
	
	public void details()
	{
		System.out.println("java");
	}
}

public class Exp2 {
	
	public static final Exp3 obj = new Exp3();
	
	public static void main(String[] args) {
		
		System.out.println("Hello");
		
		System.out.println("WORLD");
		
		Exp2.obj.details();
		Exp2.obj.show();
	}
}
