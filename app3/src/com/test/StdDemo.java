package com.test;

import java.util.Scanner;

class Student //POJO 
{
	private int id;
	private String name;
	private String city;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public Student getObj()
	{
		return new Student();
	}
}

public class StdDemo {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter id");
		
		int id = sc.nextInt();
		
		System.out.println("enter name");
		
		String name = sc.next();
		
		System.out.println("enter city");
		
		String city = sc.next();
		
		Student std = new Student();
		
		std.setId(id);
		std.setName(name);
		std.setCity(city);
		
		System.out.println(std.getId()+" "+std.getName()+" "+std.getCity());
	}
	
}
