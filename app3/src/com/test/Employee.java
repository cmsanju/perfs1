package com.test;

public class Employee {
	
	
	
	//4 types of constructors
	
	//1 default constructor
	
	public Employee()
	{
		System.out.println("Default");
	}
	
	//2 parameterised constructor
	
	public Employee(int id, String name)
	{
		System.out.println("parameterised");
	}
	
	//3 overloaded constructor
	
	public Employee(String msg, int x)
	{
		System.out.println("overloaded");
	}
	
	//4 object parameterised  constructor
	
	public Employee(Employee obj)
	{
		System.out.println("object parameterised constructor");
	}
	
	public Employee getObj(Employee emp)
	{
		System.out.println("object created");
		return emp;
	}
}
