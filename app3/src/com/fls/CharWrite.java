package com.fls;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class CharWrite {
	
	public static void main(String[] args) throws Exception
	{
		FileWriter fw = new FileWriter("src/sample.txt");
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		String msg = "this is simple write operation using char stream";
		
		bw.write(msg);
		
		bw.flush();
		
		System.out.println("Done.");
	}
}
