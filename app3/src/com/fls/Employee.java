package com.fls;

import java.io.Serializable;

public class Employee implements Serializable
{
	
	private static final long serialVersionUID = 1L;

	public int id = 101;
	
	public String name = "Rakesh";
	
	public String cmp = "Dell";
	
	public String city = "Blr";
	
	public transient int pinCode = 123123;
}
