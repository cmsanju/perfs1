package com.fls;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeSerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileInputStream fis = new FileInputStream("src/employee.txt");
		
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		Employee emp = (Employee)ois.readObject();
		
		System.out.println("ID : "+emp.id+" Name : "+emp.name+" Company : "+emp.cmp+" City : "+emp.city+" Pincode : "+emp.pinCode);
	}
}
