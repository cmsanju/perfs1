package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo 
{
	public static void main(String[] args)
	{
		try {
		
			FileOutputStream fos = new FileOutputStream("src/employee.txt");
			
			ObjectOutputStream objs = new ObjectOutputStream(fos);
			
			Employee emp = new Employee();
			
			objs.writeObject(emp);
			
			System.out.println("Done.");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
