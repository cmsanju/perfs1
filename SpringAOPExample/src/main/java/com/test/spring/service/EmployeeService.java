package com.test.spring.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.test.spring.model.Employee;

public class EmployeeService {

	@Autowired
	private Employee employee;
	
	public Employee getEmployee(){
		return this.employee;
	}
	
	public void setEmployee(Employee e){
		this.employee=e;
	}
	
	
}
