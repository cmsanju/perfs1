package com.test;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class TestDB {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session  = sf.openSession();
		
		Transaction tm = session.beginTransaction();
		
		Skills sk1 = new Skills();
		
		sk1.setS_name("Java");
		
		Skills sk2 = new Skills();
		
		sk2.setS_name("Spring");
		
		List<Skills> skl = new ArrayList<Skills>();
		
		skl.add(sk1);
		skl.add(sk2);
		
		Employee emp = new Employee();
		
		emp.setEmp_name("Rakesh");
		emp.setEmp_cmp("Dell");
		emp.setSkill(skl);
		
		session.save(emp);
		
		tm.commit();
		
		System.out.println("Done.");
		
		session.close();
	}
}
