package com.test;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

@Entity
@Table(name = "employee")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String emp_name;
	
	private String emp_cmp;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "eid")
	@OrderColumn(name="type")
	private List<Skills> skill;
	
	
	public List<Skills> getSkill() {
		return skill;
	}

	public void setSkill(List<Skills> skill) {
		this.skill = skill;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmp_name() {
		return emp_name;
	}

	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}

	public String getEmp_cmp() {
		return emp_cmp;
	}

	public void setEmp_cmp(String emp_cmp) {
		this.emp_cmp = emp_cmp;
	}
}
