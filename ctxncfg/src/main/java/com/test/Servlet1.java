package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//@WebServlet("/Servlet1")
public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		ServletContext ctx = getServletContext();
		
		String str1 = ctx.getInitParameter("name");
		String str2 = ctx.getInitParameter("clg");
		
		String str3 = str1+" "+str2;
		
		out.println(str3);
		
		ctx.setAttribute("info", str3);
		
		
		ServletConfig cfg = getServletConfig();
		
		String usr = cfg.getInitParameter("user");
		String pas = cfg.getInitParameter("pwd");
		
		out.println(usr+" "+pas);
		
		
		out.println("<a href = 'Servlet2'> next page </a>");
	}

}
