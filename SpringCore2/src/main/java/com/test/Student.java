package com.test;


public class Student {
	
	private int id;
	
	private String name;
	
	private String clg;
	
	
	private Address adr;
	
	public Student()
	{
		
	}
	
	public Student(int id, String name, String clg, Address adr)
	{
		this.id = id;
		this.name = name;
		this.clg = clg;
		this.adr = adr;
				
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClg() {
		return clg;
	}

	public void setClg(String clg) {
		this.clg = clg;
	}
	
	public Address getAdr() {
		return adr;
	}

	public void setAdr(Address adr) {
		this.adr = adr;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name+" College : "+clg);
		
		adr.details();
	}
}
