package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		Student s = ctx.getBean("std", Student.class);
		
		s.disp();
		
		Student s1 = (Student)ctx.getBean("std1");
		
		s1.disp();
	}
}
