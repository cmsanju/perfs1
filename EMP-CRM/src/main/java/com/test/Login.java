package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String usr = request.getParameter("user");
		
		String pas = request.getParameter("pwd");
		
		try
		{
			Connection con = DBCon.getCon();
			
			PreparedStatement pst = con.prepareStatement("select user_name, e_password from employee where user_name=? and e_password =?");
			
			pst.setString(1, usr);
			pst.setString(2, pas);
			
			ResultSet rs = pst.executeQuery();
			
			Employee emp1 = new Employee();
			
			while(rs.next())
			{
				emp1.setUserName(rs.getString(1));
				emp1.setPassword(rs.getString(2));
			}
			
			if(usr.equals(emp1.getUserName()) && pas.equals(emp1.getPassword()))
			{
				/*
				RequestDispatcher rd = request.getRequestDispatcher("home.html");
				
				rd.forward(request, response);
				*/
				
				//response.sendRedirect("home.html");
				response.sendRedirect("https://gmst.in/");
			}
			else
			{
				out.println("<font color ='red'>invalid credentials.</font>");
				
				RequestDispatcher rd = request.getRequestDispatcher("login.html");
				
				rd.include(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
