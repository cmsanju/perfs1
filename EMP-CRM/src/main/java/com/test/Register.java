package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("fname");
		String usr = request.getParameter("user");
		String pas = request.getParameter("pwd");
		
		Employee emp = new Employee();
		
		emp.setFullName(name);
		emp.setUserName(usr);
		emp.setPassword(pas);
		try {
		
			Connection con = DBCon.getCon();
		
			PreparedStatement pst = con.prepareStatement("insert into employee(full_name, user_name, e_password)values(?,?,?)");
		
			pst.setString(1, emp.getFullName());
			pst.setString(2, emp.getUserName());
			pst.setString(3, emp.getPassword());
			
			pst.execute();
			
			out.println("Successfully registered.");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
