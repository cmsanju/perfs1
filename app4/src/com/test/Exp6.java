package com.test;

public class Exp6 {
	
	public static void main(String[] args) {
		
		String str1 = "Hello";
		
		StringBuffer sb = new StringBuffer(str1);
		
		System.out.println(sb);
		
		sb.append(" Hi...");
		
		System.out.println(sb);
		
		System.out.println(sb.reverse());
		
		System.out.println(str1.charAt(4));
		
		for(int i = str1.length()-1; i >= 0; i--)
		{
			System.out.print(str1.charAt(i));
		}
		
		char[] ch = str1.toCharArray();
		
		String str5 = "hi hello how are you hi";
		
		String[] strAr = str5.split(" ");
		
		System.out.println(str5);
		
		for(String dt : strAr)
		{
			System.out.println(dt);
		}
	}
}
