package com.test;

public class Exp4 {
	
	public static void main(String[] args) {
		
		int x = 20;
		
		double y = 30;
		
		String str1 = String.valueOf(x);
		
		String str2 = String.valueOf(y);
		
		
		System.out.println(x+y);
		
		System.out.println(str1+str2);
		
		
		//auto boxing
		
		int j = 200;
		Integer k = j;
		
		long l = 3030;
		Long m = l;
		
		//auto un-boxing
		
		Long n = 200l;
		
		long o = n;
		
		
	}

}
