package com.test;

@FunctionalInterface
interface FunInf {
	void greet();

	default void dog() {
		
		System.out.println("default");
	}

	static void cat() {
		System.out.println("static method");
	}
}

public class Exp2 {

	public static void main(String[] args) {

		FunInf obj1 = new FunInf() 
		{
			@Override
			public void greet() {
				System.out.println("named object");
			}
		};

		obj1.greet();

		new FunInf() 
		{
			public void greet() {
				System.out.println("nameless object");
			}
		}.greet();

		//JDK 8 new features 
		
		FunInf obj2 = ()-> System.out.println("lambda expressoin");
		
		obj2.greet();
		
		obj2.dog();
		
		FunInf.cat();

	}
}
