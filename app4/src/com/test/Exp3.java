package com.test;

@FunctionalInterface
interface FunInf2
{
	String userName(String name);
	
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		FunInf2 obj = (String name)-> 
		{
			System.out.println("UserName : "+name);
			
			return name;
		};
		
		obj.userName("Rama");
	}

}
