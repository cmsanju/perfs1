package com.test;

interface Inf2
{
	void add();
	
	interface Inf3{
	
	void mul();
	}
}

class Impl1 implements Inf2.Inf3
{
	public void mul()
	{
		System.out.println("mul()");
	}
	
	
}

public class Exp1 {
	public static void main(String[] args) {
		
		Impl1 obj = new Impl1();
		
		obj.mul();
	}
}
