package com.test;

public class Exp5 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		String str2 = "test";
		String str3 = "java";
		
		String str4 = new String("java");
		
		System.out.println(str1 == str3);
		System.out.println(str1 == str4);
		
		System.out.println(str1+" "+str4);
		
		System.out.println(str1.equals(str4));
		System.out.println(str1 == str2);
		System.out.println(str1.equals(str2));
		
		String str5 = str1.concat(" collections");
		
		System.out.println(str5);
		
		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
		
		String s = "aba";
	}
}
