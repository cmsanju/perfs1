package com.ths;

public class Exp2 implements Runnable
{
	@Override
	public void run()
	{
		System.out.println("run() : "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		
		Exp2 t1 = new Exp2();
		
		ThreadGroup tg1 = new ThreadGroup("Maths");
		
		Thread t2 = new Thread(tg1, t1, "Add");
		
		Thread t3 = new Thread(tg1, t1, "Sub");
		
		Thread t4 = new Thread(tg1, t1, "Div");
		
		ThreadGroup tg2 = new ThreadGroup("Bank");
		
		Thread t5 = new Thread(tg2, t1, "Transfer");
		
		Thread t6 = new Thread(tg2, t1, "Withdraw");
		
		Thread t7 = new Thread(tg2, t1, "Credit");
		
		t2.start();
		t4.start();
		System.out.println("TG1 : "+tg1.activeCount());
		
		t7.start();
		t6.start();
		System.out.println("TG2 : "+tg2.activeCount());
	}
}
