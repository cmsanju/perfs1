package com.ths;

class Add
{
	public void add()
	{
		System.out.println("add()");
	}
}

class Sub
{
	public void sub()
	{
		System.out.println("sub()");
	}
}

public class Exp1 implements Runnable
{

	@Override
	public void run() {
		
		try {
			Add a = new Add();
			a.add();
			System.out.println("run()");
			Thread.sleep(2000);
			Sub s = new Sub();
			s.sub();
		}
		catch(Exception e)
		{
			
		}
	}
	
	public static void main(String[] args) {
		
		Exp1 obj = new Exp1();
		
		Thread t1 = new Thread(obj, "Add");//converting Runnable interface object to thread class
		
		t1.start();
		
		Thread t2 = new Thread(obj, "Sub");
		Thread t3 = new Thread(obj, "Div");
		Thread t4 = new Thread(obj, "Mul");
		
		System.out.println(" thread name : "+t1.getName());
		System.out.println(" thread name : "+t2.getName());
		System.out.println(" thread name : "+t3.getName());
		System.out.println(" thread name : "+t4.getName());
		
		t1.setPriority(10);
		System.out.println("Default thread priority : "+t1.getPriority());
		System.out.println("Default thread priority : "+t2.getPriority());
		System.out.println("Default thread priority : "+t3.getPriority());
		System.out.println("Default thread priority : "+t4.getPriority());
	}

}
