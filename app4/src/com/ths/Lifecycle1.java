package com.ths;


public class Lifecycle1 extends Thread
{
	@Override
	public void run()
	{
		try
		{
			Thread.sleep(1000);
			System.out.println("run()");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		
		Lifecycle1 t1 = new Lifecycle1();
		
		System.out.println("Before starting thread status : "+t1.isAlive());
		System.out.println("Before starting thread state : "+t1.getState());
		
		t1.start();
		
		System.out.println("After starting thread status : "+t1.isAlive());
		System.out.println("After starting thread state : "+t1.getState());
		
		Thread.sleep(500);
		
		System.out.println("in sleep thread status : "+t1.isAlive());
		System.out.println("in sleep thread state : "+t1.getState());
		
		t1.join();
		
		System.out.println("after join thread status : "+t1.isAlive());
		System.out.println("after join thread state : "+t1.getState());
		
		System.out.println(MAX_PRIORITY);//10
		System.out.println(NORM_PRIORITY);//5
		System.out.println(MIN_PRIORITY);//1
	}
}
