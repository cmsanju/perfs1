package com.emp;

import java.util.Scanner;

public class Test {
	
	public static void main(String[] args) //throws PsalException,NsalException
	{
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter salary");
		
		double sal = sc.nextDouble();
		
		ValidateSalary vsal = new ValidateSalary();
		
		
		try {
		
		vsal.checkSal(sal);
		
		}
		catch(PsalException pe)
		{
			System.out.println("positive salary");
		}
		catch(NsalException ne)
		{
			System.out.println("negative salary");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
