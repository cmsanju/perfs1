package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		
		
		try
		{
			System.out.println(100/4);
			
			int[] ar = {10,20,30,40};
			
			System.out.println(ar[3]);
			
			int a = Integer.parseInt("sw33");
			
		}
		
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check your array size");
		}
		catch (NumberFormatException nfe) {
			
			System.out.println("enter only numerics for int type");
		}
		catch(NullPointerException npe)
		{
			System.out.println("enter string input");
		}
		catch(Exception e)
		{
			System.out.println("check inputs");
		}
		
		finally
		{
			System.out.println("i am from finally.");
			
			try {
				
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
	}

}
