package com.excp;

public class Exp3 {
	
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println("test");
			System.out.println(10/0);
			System.out.println();
		}
		catch(Exception e)
		{
			//using getMessage() only exception message
			System.out.println(e.getMessage());
			
			//printing exception class object it will print class name and message
			System.out.println(e);
			
			//using printStackTrace() it give class name exception message and line number
			
			e.printStackTrace();
		}
	}
}
