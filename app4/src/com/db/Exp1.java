package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");//oracle.jdbc.driver.OracleDriver
		
		//2 create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mngB1", "root","password");
		
		//3 create statement object
		Statement stmt = con.createStatement();
		
		//4 execute query
		//stmt.execute("create table employee(id int, emp_name varchar(50), company varchar(50))");
		
		String sql1 = "insert into employee values(1, 'Rama', 'Bharat')";
		String sql2 = "insert into employee values(2, 'Nitesh', 'lenovo')";
		String sql3 = "insert into employee values(3, 'Gupta', 'Genpct')";
		String sql4 = "insert into employee values(4, 'Shivam', 'Dell')";
		String sql5 = "insert into employee values(5, 'Ranjan', 'IBM')";
		String sql6 = "update employee set emp_name = 'Rakesh', company = 'PWC' where id =1 ";
		String sql7 = "delete from employee where id = 5";
		//stmt.addBatch(sql1);
		//stmt.addBatch(sql2);
		//stmt.addBatch(sql3);
		//stmt.addBatch(sql4);
		//stmt.addBatch(sql5);
		stmt.addBatch(sql6);
		stmt.addBatch(sql7);
		
		stmt.executeBatch();
		
		//5 close the connection object
		con.close();
		
		System.out.println("Done.");
	}

}
