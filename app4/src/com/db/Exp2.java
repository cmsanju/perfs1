package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mngB1", "root", "password");
		
		//Statement stmt = con.createStatement();
		
		/*
		PreparedStatement pst2 = con.prepareStatement("insert into employee values(?,?,?)");
		
		pst2.setInt(1, 5);
		pst2.setString(2, "King");
		pst2.setString(3, "E Y");
		
		pst2.execute();
		
		
		PreparedStatement pst3 = con.prepareStatement("delete from employee where id = ?");
		
		pst3.setInt(1, 5);
		
		pst3.execute();
		
		*/
		
		PreparedStatement pst4 = con.prepareStatement("update employee set emp_name =?, company =? where id =? ");
		
		pst4.setString(1, "Ramanuj");
		pst4.setString(2, "DXC");
		pst4.setInt(3, 4);
		
		pst4.execute();
		
		String sql = "select * from employee";
		
		PreparedStatement pst1 = con.prepareStatement(sql);
		
		
		
		
		
		ResultSet rs = pst1.executeQuery(sql);
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" Company : "+rs.getString(3));
		}
		
		
	}
}
