package com.cls;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

class Product
{
	private int id;
	
	private String name;
	
	private int price;
	
	public Product()
	{
		
	}
	
	public Product(int id, String name, int price)
	{
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, price);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return id == other.id && Objects.equals(name, other.name) && price == other.price;
	}
	
	
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		HashSet<Product> prData = new HashSet<>();
		
		prData.add(new Product(5, "apple", 3000));
		prData.add(new Product(5, "apple", 3000));
		prData.add(new Product(5, "apple", 3000));
		prData.add(new Product(5, "apple", 3000));
		prData.add(new Product(5, "apple", 3000));
		
		for(Product pr : prData)
		{
			System.out.println(pr.getId()+" "+pr.getName()+" "+pr.getPrice());
		}
		
		HashMap<Product, String> prMap = new HashMap<Product, String>();
		
		prMap.put(new Product(5, "apple", 3000), "pr1");
		prMap.put(new Product(5, "apple", 3000), "pr2");
		prMap.put(new Product(5, "apple", 3000), "pr3");
		prMap.put(new Product(5, "apple", 3000), "pr4");
		
		for(Product pr : prMap.keySet())
		{
			System.out.println(pr.getId()+" "+pr.getName()+" "+pr.getPrice()+" "+prMap.get(pr));
		}
	}
}
