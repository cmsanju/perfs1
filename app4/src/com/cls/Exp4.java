package com.cls;

import java.util.Iterator;
import java.util.TreeSet;

public class Exp4 {
	
	public static void main(String[] args) {
		
		TreeSet<String> data = new TreeSet<>();
		
		data.add("java");
		data.add("c");
		data.add("go");
		data.add("spring");
		data.add("python");
		data.add("php");
		data.add("perl");
		data.add("jdbc");
		data.add("servlets");
		
		System.out.println(data);
		
		Iterator<String> itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		TreeSet<Integer> dt = new TreeSet<>();
		
		dt.add(100);
		dt.add(39);
		dt.add(40);
		dt.add(20);
		dt.add(25);
		dt.add(15);
		dt.add(7);
		dt.add(9);
		
		System.out.println(dt);
		
		Iterator<Integer> it = dt.iterator();
		
		while(it.hasNext())
		{
			System.out.println(it.next());
		}
	}
}
