package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Set data = new LinkedHashSet();
		
		data.add(10);
		data.add("java");
		data.add(30);
		data.add("test");
		data.add(10);
		data.add('A');
		data.add(false);
		data.add(49.44);
		data.add(23.45f);
		data.add("java");
		
		System.out.println(data);
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}
}
