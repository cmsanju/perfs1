package com.cls;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Exp5 {
	
	public static void main(String[] args) {
		
		//LinkedHashMap<String, Integer> data = new LinkedHashMap<>();
		
		//TreeMap<String, Integer> data = new TreeMap<>();
		
		//HashMap<String, Integer> data = new HashMap<String, Integer>();
		
		Hashtable<String, Integer> data = new Hashtable<String, Integer>();
		
		data.put("lenovo", 300);
		data.put("sony", 350);
		data.put("asus", 250);
		data.put("dell", 300);
		data.put("apple", 2345);
		data.put("mac", 300);
		data.put("iPad",3445);
		data.put("sony", 400);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Product : "+et.getKey()+" Price : "+et.getValue());
		}
		
		for(String k : data.keySet())
		{
			System.out.println("Key : "+k+" Value : "+data.get(k));
		}
		
		//java 8 new feature forEach()
		
		data.forEach((k,v) -> System.out.println(k+" "+v));
	}
}
