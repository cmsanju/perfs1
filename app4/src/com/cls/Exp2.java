package com.cls;

import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();//LIFO
		
		data.add(10);
		data.add("java");
		data.add(30);
		data.add("test");
		data.add(10);
		data.add('A');
		data.add(false);
		data.add(49.44);
		data.add(23.45f);
		data.add("java");
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		data.push("King");
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		System.out.println(data.search(10));
		
		data.clear();
		
		System.out.println(data.empty());
	}
}
