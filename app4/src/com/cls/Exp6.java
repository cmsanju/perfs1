package com.cls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

class Employee implements Comparable<Employee>
{
	private int id;
	
	private String name;
	
	private int salary;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, int salary)
	{
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
 /*
	@Override
	public int compareTo(Employee o) {
		
		return this.id-o.id;
	}
 */
	
	@Override
	public int compareTo(Employee o) {
		
		return o.salary-this.salary;
	}
}

class NameCoparator implements Comparator<Employee>
{

	@Override
	public int compare(Employee o1, Employee o2) {
		
		return o1.getName().compareTo(o2.getName());
	}
	
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		ArrayList<Employee> empList = new ArrayList<>();
		
		empList.add(new Employee(11, "hero", 234));
		empList.add(new Employee(7, "nitesh", 339));
		empList.add(new Employee(15, "ranjan", 123));
		empList.add(new Employee(5, "rakesh", 202));
		empList.add(new Employee(3, "shivam", 822));
		/*
		System.out.println("Before sorting the data");
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getSalary());
		}
		
		Collections.sort(empList, new NameCoparator());
		
		System.out.println("After sorting the data");
		
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getSalary());
		}
		*/
		//using stream api ascending order sorting
		empList.stream()
		.sorted(Comparator.comparing(Employee :: getSalary))
		.forEach(emp ->System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getSalary()));;
		
		System.out.println("=============");
		//using stream api descending order sorting
		
		empList.stream().sorted(Comparator.comparing(Employee :: getSalary).reversed())
		.forEach(emp -> System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getSalary()));
		
		System.out.println("**************");
		
		empList.stream().filter(emp -> emp.getName().startsWith("n"))
		.forEach(emp -> System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getSalary()));
	}
}
