package com.cls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//Collection data = new ArrayList();
		
		//List data = new ArrayList();
		
		//ArrayList data = new ArrayList();
		
		LinkedList data = new LinkedList();
		
		data.add(10);
		data.add("java");
		data.add(30);
		data.add("test");
		data.add(10);
		data.add('A');
		data.add(false);
		data.add(49.44);
		data.add(23.45f);
		data.add("java");
		
		System.out.println(data.contains(100));
		
		System.out.println(data.size());
		
		data.set(3, "Hello");
		//data.remove(3);
		System.out.println(data.get(4));
		
		System.out.println(data);
		
		//Iterator itr = data.iterator(); forward direction
		ListIterator ltr = data.listIterator();
		
		while(ltr.hasNext())
		{
			System.out.println(ltr.next());
		}
		
		System.out.println("=========");
		
		while(ltr.hasPrevious())
		{
			System.out.println(ltr.previous());
		}
	}
}
/*
 * CRUD
 * 
 * C - CREATE
 * R - READ
 * U - UPDATE
 * D - DELETE
 */
