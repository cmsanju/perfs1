package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream obj = new ObjectOutputStream(fos);
		
		Employee emp = new Employee();
		
		obj.writeObject(emp);
		
		System.out.println("Done.");
	}
}
