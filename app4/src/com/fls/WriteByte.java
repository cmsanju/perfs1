package com.fls;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class WriteByte {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/write.txt");
		
		FileOutputStream fout = new FileOutputStream(file);
		
		BufferedOutputStream br = new BufferedOutputStream(fout);
		
		String msg = "Simple ByteStream write operation";
		
		br.write(msg.getBytes());
		
		System.out.println("Done.");
	}
}
