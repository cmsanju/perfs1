package com.fls;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class WriteChar {
	
	public static void main(String[] args) throws Exception
	{
		
		FileWriter fw = new FileWriter("src/sample.txt");
		
		BufferedWriter bw = new BufferedWriter(fw);
		
		String msg = "Char Stream write operation simple text";
		
		bw.write(msg);
		
		bw.flush();
		
		System.out.println("Done.");
	}
}
