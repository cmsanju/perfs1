package p1;

public class Exp1 {
	
	private int a = 100;
	
			int b = 40;
			
	protected int c = 500;
	
	public int d = 700;
	
	public void disp()
	{
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
	
	public static void main(String[] args) {
		
		Exp1 obj = new Exp1();
		
		obj.disp();
	}
}

class Exp2 extends Exp1
{
	public void disp()
	{
		//System.out.println(a);//private
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
}
