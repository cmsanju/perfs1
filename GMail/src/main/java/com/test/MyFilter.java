package com.test;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;


@WebFilter("/Loign")
public class MyFilter extends HttpFilter implements Filter {
       
    
	public void destroy() {
		
	}

	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		System.out.println("Before servlet");
		// pass the request along the filter chain
		chain.doFilter(request, response);
		
		System.out.println("after servlet");
	}

	
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
