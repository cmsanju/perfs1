package com.ths;

public class Exp1 extends Thread
{
	@Override
	public void run()
	{
		System.out.println("run() "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		
		Exp1 t1 = new Exp1();
		Exp1 t2 = new Exp1();
		Exp1 t3 = new Exp1();
		
		System.out.println(t1.getName());
		System.out.println(t2.getName());
		System.out.println(t3.getName());
		
		t1.setName("Transfer");
		t2.setName("Withdraw");
		t3.setName("Credit");
		//t2.start();
		
		System.out.println(t1.getName());
		System.out.println(t2.getName());
		System.out.println(t3.getName());
		
		System.out.println(t1.getPriority());
		System.out.println(t2.getPriority());
		System.out.println(t3.getPriority());
		
		System.out.println(MAX_PRIORITY);
		System.out.println(NORM_PRIORITY);
		System.out.println(MIN_PRIORITY);
		
		t1.setPriority(MAX_PRIORITY);
		t3.setPriority(MIN_PRIORITY);
		
		System.out.println("T1 : "+t1.getPriority());
		System.out.println("T3 : "+t3.getPriority());
	}
}
