package com.ths;

class Forward
{
	public void printDtFw()
	{
		try {
			for(int i = 0; i <= 10; i++)
			{
				Thread.sleep(1000);
				System.out.println(i);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

class Reverse
{
	public void printDtRv() {
		
		try {
		
			for(int i = 10; i >= 0; i--)
			{
				Thread.sleep(1000);
				System.out.println(i);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}

public class Exp2 implements Runnable
{
	Forward fw = new Forward();
	Reverse rs = new Reverse();
	
	@Override
	public void run()
	{
	  try
	  {
		  fw.printDtFw();
		  Thread.sleep(1000);
		  rs.printDtRv();
	  }
	  catch(Exception e)
	  {
		  e.printStackTrace();
	  }
	}
	
	public static void main(String[] args) {
		
		Exp2 t1 = new Exp2();
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		Thread t2 = new Thread(tg1, t1,"Transfer");
		Thread t3 = new Thread(tg1, t1,"Withdraw");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
		
		Thread t4 = new Thread(tg2, t1,"Add");
		Thread t5 = new Thread(tg2, t1,"Sub");
		
		t2.start();
		System.out.println(tg1.activeCount());
		t5.start();
		System.out.println(tg2.activeCount());
	}
}
