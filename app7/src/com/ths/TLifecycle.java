package com.ths;

public class TLifecycle extends Thread
{
	@Override
    public void run()
    {
	  try
	  {
		  Thread.sleep(2000);
		  System.out.println("run()"); 
	  }
	  catch(Exception e)
	  {
		  e.printStackTrace();
	  }
    }
	
	public static void main(String[] args) throws Exception
	{
		
		TLifecycle t1 = new TLifecycle();
		
		System.out.println("Before starting thread state : "+t1.getState());
		System.out.println("Before starting thread status : "+t1.isAlive());
		t1.start();
		System.out.println("After starting thread state : "+t1.getState());
		System.out.println("After starting thread status : "+t1.isAlive());
		Thread.sleep(1000);
		System.out.println("In sleep thread state : "+t1.getState());
		System.out.println("In sleep thread status : "+t1.isAlive());
		
		t1.join();
		System.out.println("After join thread state : "+t1.getState());
		System.out.println("After join thread status : "+t1.isAlive());
	}
}
