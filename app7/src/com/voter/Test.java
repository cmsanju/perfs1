package com.voter;

import java.util.Scanner;

public class Test {
	
	public static void main(String[] args) {
		
		try {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter age");
		
		int age = sc.nextInt();
		
		ValidateAge v = new ValidateAge();
		
		v.checkAge(age);
		
		}
		catch(ValidAgeException ve)
		{
			System.err.println("age matching");
		}
		catch(InvalidAgeException ie) 
		{
			System.err.println("not eligible");
		}
	}

}
