package com.test;

@FunctionalInterface
interface FunInf
{
	String greet();
	
	default void disp()
	{
		System.out.println("Test");
	}
	
	static void show()
	{
		System.out.println("static method");
	}
}

public class Exp4 {
	
	public static void main(String[] args) {
		/*
		FunInf obj = new FunInf()
				{
					@Override
					public String greet()
					{
						System.out.println("override");
						
						return "aks";
					}
				};
				
				obj.disp();
				obj.greet();
				
				FunInf.show();
				
				new FunInf()
				{
					@Override
					public String greet()
					{
						System.out.println("nameless");
						
						return "hello";
					}
				}.greet();
				*/
				//java 8 new feature lambda expression
				
				FunInf obj1 = ()->{
					System.out.println("lambda");
					
					return "java";
				};
				
				obj1.greet();
				obj1.disp();
	}
}
