package com.test;

public class Exp5 {
	
	public static void main(String[] args) {
		
		String v1 = "200";
		
		int x = Integer.parseInt(v1);
		
		int j = 300;
		
		String k = String.valueOf(j);
		
		//auto boxing
		int a = 300;
		
		Integer aa = new Integer(a);
		
		float f = 93.33f;
		
		Float ff = new Float(f);
		
		//auto-unboxing
		
		Double d = new Double(373);
		
		double dd=d;
		
		Long l = new Long(323);
		
		long ll = l;
	}
}
