package com.test;

public class Exp6 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		String str3 = "java";
		String str4 = "java";
		
		String str2 = new String("java");
		
		System.out.println(str1 == str3);
		
		System.out.println(str1.equals(str2));
		
		str1.concat(" files");
		
		System.out.println(str1);
		
		for(int i = str1.length()-1; i >= 0; i--)
		{
			System.out.print(str1.charAt(i));
		}
		
		StringBuffer sb = new StringBuffer(str1);
		
		System.out.println(sb);
		
		System.out.println(sb.reverse());
		
		sb.append(" Spring");
		
		System.out.println(sb);
	}
}
