package com.test;

interface D
{
	private static void human()
	{
		System.out.println("private method");
	}
	
	default void animal()
	{
		D.human();
		System.out.println("animal");
	}
	
	static void books()
	{
		System.out.println("static books");
		human();
	}
	
	void dog();
}

interface G extends D
{
	void house();
	void college();
}

abstract class E implements D
{
	public void fox()
	{
		System.out.println("fox()");
	}
}

class F extends E
{
	@Override
	public void dog()
	{
		System.out.println("inf dog");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		F obj = new F();
		
		obj.animal();
		obj.dog();
		
		D.books();
		//D.human();
	}
}
