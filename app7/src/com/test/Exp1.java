package com.test;

interface A
{
	void show();
	void disp();
}

abstract class B
{
	public abstract void cat();
	
	public void books()
	{
		System.out.println("abs normal method");
	}
}

class C extends B implements A
{
	public void animal()
	{
		System.out.println("animal");
	}
	
	@Override
	public void cat()
	{
		System.out.println("abs cat()");
	}
	
	@Override
	public void show()
	{
		System.out.println("inf show()");
	}
	@Override
	public void disp() {
		System.out.println("inf disp()");
	}
}

public class Exp1 {
	
	public static void main(String[] args) {
		
		//A a = new A();
		
		//B b = new B();
		
		C c = new C();
		
		c.animal();
		c.books();
		c.cat();
		c.show();
		c.disp();
	}
}
