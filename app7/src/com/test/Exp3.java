package com.test;

interface Inf1
{
	void add();
	
	interface Inf2
	{
	 void sub();
	}
}

class Impl implements Inf1.Inf2
{
	@Override
	public void sub()
	{
		System.out.println("nested interfaces");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
	
		Impl obj = new Impl();
		
		obj.sub();
	}
}
