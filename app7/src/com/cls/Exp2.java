package com.cls;

import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add("java");
		data.add('w');
		data.add("java");
		data.add(10);
		data.add(22.22);
		data.add(939.33f);
		data.add(3929229l);
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		System.out.println(data);
		
		data.push("hello");
		
		System.out.println(data);
		
		System.out.println(data.search(100));
		data.clear();
		System.out.println(data.empty());
	}
}
