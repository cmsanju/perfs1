package com.cls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Employee implements Comparable<Employee>
{
	private int id;
	
	private String name;
	
	private int age;
	
	private int salary;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, int age, int salary)
	{
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public int compareTo(Employee o) {
		
		return this.salary - o.getSalary();
	}
}

class NameComparator implements Comparator<Employee>
{

	@Override
	public int compare(Employee o1, Employee o2) {
		
		return o1.getName().compareTo(o2.getName());
	}
	
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		ArrayList<Employee> empList = new ArrayList<>();
		
		empList.add(new Employee(5, "rakesh", 25, 534));
		empList.add(new Employee(2, "ranjan", 22,343));
		empList.add(new Employee(7, "gupta", 30, 8384));
		empList.add(new Employee(4, "shivam", 82, 3922));
		empList.add(new Employee(1, "nitesh", 62, 3828));
		/*
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getAge()+" "+emp.getSalary());
		}
		
		NameComparator nameObj = new NameComparator();
		
		Collections.sort(empList, nameObj);
		
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getAge()+" "+emp.getSalary());
		}
		
		Comparator<Employee> eobj = Comparator.comparing(Employee :: getName).thenComparing(Employee :: getSalary);
		
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getAge()+" "+emp.getSalary());
		}
		
		empList.sort(Comparator.comparing(Employee :: getSalary).thenComparing(Employee :: getAge));
		
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getAge()+" "+emp.getSalary());
		}
		*/
		
		empList.sort(Comparator.comparing(Employee :: getName).reversed()
				.thenComparing(Comparator.comparing(Employee :: getSalary).reversed()));
		for(Employee emp : empList)
		{
			System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getAge()+" "+emp.getSalary());
		}
	}
}
