package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Set<String> data = new TreeSet<String>();
		
		data.add("java");
		data.add("c");
		data.add("groovy");
		data.add("angular");
		data.add("python");
		data.add("angularJS");
		data.add("java");
		data.add("kotlin");
		data.add("apple");
		
		System.out.println(data);
		
		Iterator<String> itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
	}
}
