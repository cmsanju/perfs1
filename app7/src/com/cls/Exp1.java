package com.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class Exp1 {
	
	public static void main(String[] args) {
	
		//List list = new List();
		//List data = new ArrayList();
		//List data = new LinkedList();
			Vector data = new Vector();
		data.add(10);
		data.add("java");
		data.add('w');
		data.add("java");
		data.add(10);
		data.add(22.22);
		data.add(939.33f);
		data.add(3929229l);
		
		System.out.println(data.size());
		
		Iterator itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
	}
}
