package com.cls;

import java.util.ArrayList;
import java.util.List;

public class Exp6 {
	
	public static void main(String[] args) {
		
		List<String> data = new ArrayList<String>();
		
		data.add("java");
		data.add("c");
		data.add("groovy");
		data.add("angular");
		data.add("python");
		data.add("angularJS");
		data.add("java");
		data.add("kotlin");
		data.add("apple");
		
		System.out.println(data);
		
		data.stream().filter(k -> k.startsWith("a")).sorted().forEach(x -> System.out.println(x));
		
	}
}
