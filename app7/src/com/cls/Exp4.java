package com.cls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Exp4 {
	
	public static void main(String[] args) {
		
		Map<String, Integer> data = new TreeMap<>();
		
		data.put("lenovo", 202);
		data.put("asus",244);
		data.put("dell", 833);
		data.put("samsung", 232);
		data.put("mac", 3344);
		data.put("ideapad", 2338);
		data.put("apple", 7484);
		data.put("dell", 550);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		/*
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println(et.getKey()+" "+et.getValue());
		}
		
		for(String k : data.keySet())
		{
			System.out.println(k+" "+data.get(k));
		}
		*/
		data.forEach((k,v)->System.out.println(k+" "+v));
	}
}
