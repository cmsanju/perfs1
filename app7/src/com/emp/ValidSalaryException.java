package com.emp;

public class ValidSalaryException extends Exception
{
	public ValidSalaryException(String msg)
	{
		super(msg);
	}
}
