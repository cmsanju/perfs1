package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.ResultSetMetaData;

public class Exp2 {

	public static void main(String[] args) throws Exception
	{

		// load the driver class
		Class.forName("com.mysql.jdbc.Driver");

		// create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbmng", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into student values(?,?,?)");
		
		pst.setInt(1, 8);
		pst.setString(2, "Sunil");
		pst.setString(3, "SKIT");
		
		pst.execute();
		
		
		PreparedStatement pst = con.prepareStatement("update student set std_name =?, std_clg =? where id =? ");
		
		pst.setString(1, "Krunal");
		pst.setString(2, "SVU");
		pst.setInt(3, 8);
		
		pst.execute();
		
		
		
		PreparedStatement pst = con.prepareStatement("delete from student where id =? ");
		
		pst.setInt(1, 8);
		
		pst.execute();
		*/
		
		PreparedStatement pst = con.prepareStatement("select * from student");
		
		ResultSet rs = pst.executeQuery();
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" College : "+rs.getString(3));
		}
		
		ResultSetMetaData rsd = rs.getMetaData();
		
		System.out.println(rsd.getColumnCount());
		System.out.println(rsd.getColumnName(2));
	}
}
