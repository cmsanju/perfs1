package com.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class Exp3 {

	public static void main(String[] args) throws Exception
	{

		// load the driver class
		Class.forName("com.mysql.jdbc.Driver");

		// create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbmng", "root", "password");
		
		CallableStatement cst = con.prepareCall("{call testRead()}");
		
		ResultSet rs = cst.executeQuery();
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" College : "+rs.getString(3));
		}
	}
}
