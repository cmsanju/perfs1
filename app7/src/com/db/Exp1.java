package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		//create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbmng", "root", "password");
		
		con.setAutoCommit(false);
		
		//create statement object
		Statement stmt = con.createStatement();
		
		String sql1 = "insert into student values(6, 'Gates', 'MNTS')";
		String sql4 = "insert into student values(7, 'kING', 'BLR')";
		
		String sql2 = "update student set std_name = 'Hero', std_clg = 'KKTS' where id = 5";
		
		String sql3 = "delete from student where id = 5";
		
		
		
		String sql5 = "select * from student";
		
		//execute query
		//stmt.execute(sql);
		stmt.addBatch(sql1);
		stmt.addBatch(sql4);
		stmt.addBatch(sql2);
		stmt.addBatch(sql3);
		
		stmt.executeBatch();
		
		//con.commit();
		con.rollback();
		
		ResultSet rs = stmt.executeQuery(sql5);
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" College : "+rs.getString(3));
		}
		
		
		//close the connection object
		con.close();
	}
}
