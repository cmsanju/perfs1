package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fout = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream obj1 = new ObjectOutputStream(fout);
		
		Employee e = new Employee();
		
		obj1.writeObject(e);
		
		System.out.println("Done");
	}
}
