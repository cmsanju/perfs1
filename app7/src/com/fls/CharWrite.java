package com.fls;

import java.io.FileWriter;

public class CharWrite {
	
	public static void main(String[] args) throws Exception
	{
		
		FileWriter fw = new FileWriter("src/xyz.txt");
		
		String msg = "This is simple char stream write operation";
		
		fw.write(msg);
		
		fw.flush();
		
		System.out.println("Done");
	}
}
