package com.fls;

import java.io.Serializable;

public class Employee implements Serializable
{
	
	public int id = 101;
	
	public String name = "Gupta";
	
	public String cmp = "Dell";
	
	public String city = "Blr";
	
	public transient int pinCode = 123123;
}
