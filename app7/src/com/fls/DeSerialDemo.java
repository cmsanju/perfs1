package com.fls;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeSerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileInputStream fis = new FileInputStream("src/employee.txt");
		
		ObjectInputStream obj1 = new ObjectInputStream(fis);
		
		Employee emp = (Employee)obj1.readObject();
		
		System.out.println(emp.id+" "+emp.name+" "+emp.cmp+" "+emp.city+" "+emp.pinCode);
	}
}
