package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class ByteWrite {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/abc.txt");
		
		FileOutputStream fout = new FileOutputStream(file);
		
		String msg = "Hi this is simple byte stream write operation";
		
		fout.write(msg.getBytes());
		
		System.out.println("Done");
	}
}
