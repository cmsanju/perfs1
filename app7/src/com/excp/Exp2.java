package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(30/0);
		}
		catch(Exception e)
		{
			//1 using getMessage()
			System.out.println(e.getMessage());
			
			//2 printing exception class object
			System.out.println(e);
			
			//3 using printStackTrace()
			e.printStackTrace();
		}
	}
}
