package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		try
		{

			System.out.println("test");
			
			System.out.println(1001/5);
			
			System.out.println("java");
			
			int[] arr = {10,20,30,40};
			
			System.out.println(arr[1]);
			
			String str = "java";
			
			System.out.println(str.charAt(2));
			
			String name = "hello";
			
			System.out.println(name.charAt(0));
		}
		
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check array length");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check string length");
		}
		catch(NullPointerException npe)
		{
			System.out.println("enter string input");
		}
		catch(Exception e)
		{
			System.out.println("check inputs");
		}
		
		finally
		{
			System.out.println("finally block");
		}
	}
}
