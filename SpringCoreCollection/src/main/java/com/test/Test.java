package com.test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		/*
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		Employee e = (Employee)ctx.getBean("emp");
		
		e.disp();
		*/
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AnnConfig.class);
		
		Employee e = ctx.getBean(Employee.class);
		
		e.setId(101);
		e.setName("Shivam");
		
		List<String> ls = new ArrayList<String>();
		ls.add("java");
		ls.add("Spring");
		ls.add("aws");
		
		e.setSkill(ls);
		
		e.disp();
	}
}
