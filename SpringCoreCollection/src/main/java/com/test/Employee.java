package com.test;

import java.util.List;

public class Employee {
	
	private int id;
	
	private String name;
	
	private List<String> skill;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, List<String> skill)
	{
		this.id = id;
		this.name = name;
		this.skill = skill;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getSkill() {
		return skill;
	}

	public void setSkill(List<String> skill) {
		this.skill = skill;
	}
	
	public void disp()
	{
		System.out.println("ID : "+id+" Name : "+name);
		System.out.println("Skills : ");
		
		for(String dt : skill)
		{
			System.out.println(dt);
		}
	}
}
