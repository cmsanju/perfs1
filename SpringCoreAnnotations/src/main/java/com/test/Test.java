package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(CfgTest.class);
		
		TextMessage tm =  ctx.getBean(TextMessage.class);
		EmailMessage em =  ctx.getBean(EmailMessage.class);
		
		System.out.println(tm.message("Text Message"));
		System.out.println(em.message("Email Message"));
	}
}
