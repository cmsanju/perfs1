package com.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CfgTest {
	
	@Bean
	public MessageService objTextMsg()
	{
		return new TextMessage();
	}
	
	@Bean
	public MessageService objEmailMsg()
	{
		return new EmailMessage();
	}
}
