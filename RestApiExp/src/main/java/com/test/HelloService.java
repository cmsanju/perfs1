package com.test;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloService {
	
	@GET
	@Path("/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String sayHelloPlainText()
	{
		return "Hi this is simple plain text response using jersey api";
	}
	
	@GET
	@Path("/html/{name}")
	@Produces(MediaType.TEXT_HTML)
	public String sayHelloHtml(@PathParam("name") String name)
	{
		return "<hmtl> <body><h2>Hi this is plain HTML response : "+name+" </h2></body></html>";
	}
}
/*
 * CREATE : POST : INSERT
 * READ : GET : SELECT 
 * UPDATE : PUT : UPDATE
 * DELETE : DELETE : DELETE
 * 
 */
